from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from exchange_api.serializers.login import LoginSerializer, LoginResponseSerializer


class LoginAPIView(GenericAPIView):
    permission_classes = ()
    serializer_class = LoginSerializer

    @swagger_auto_schema(
        operation_summary="Login user", responses={200: LoginResponseSerializer}
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return Response(LoginResponseSerializer(instance).data)
