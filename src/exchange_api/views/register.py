from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from exchange_api.serializers.register import RegisterSerializer
from exchange_api.serializers.user import UserSerializer


class RegisterAPIView(GenericAPIView):
    serializer_class = RegisterSerializer
    permission_classes = ()

    @swagger_auto_schema(
        operation_summary="Register user",
        request_body=RegisterSerializer,
        responses={201: UserSerializer},
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return Response(UserSerializer(instance).data, status=201)
