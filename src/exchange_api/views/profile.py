from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from exchange_api.serializers.transfer import TransferCreateSerializer
from exchange_api.serializers.user import UserOperationSerializer


class UserViewSet(GenericViewSet):
    def get_serializer_class(self):
        if self.action == "transfer":
            return TransferCreateSerializer
        return UserOperationSerializer

    @swagger_auto_schema(
        methods=["POST"],
        operation_summary="Transfer money",
        request_body=TransferCreateSerializer,
        responses={200: ""},
    )
    @action(methods=["POST"], detail=False)
    def transfer(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=200)

    @swagger_auto_schema(
        methods=["GET"],
        operation_summary="Get list of operations",
        responses={200: UserOperationSerializer},
    )
    @action(methods=["GET"], detail=False)
    def operations(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.request.user)
        return Response(serializer.data)
