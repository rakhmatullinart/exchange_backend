import factory.fuzzy

from exchange_api.models import User, Currency


class CurrencyFactory(factory.DjangoModelFactory):
    class Meta:
        model = Currency

    code = factory.faker.Faker("currency_code")
    is_base_currency = False
    multiplicity = 100
    rate = factory.fuzzy.FuzzyInteger(100)


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.faker.Faker("email")
    currency = factory.SubFactory(CurrencyFactory)
    balance = factory.fuzzy.FuzzyInteger(100, 1000)
