import pytest
from rest_framework.test import APIClient

from exchange_api.tests.factories import UserFactory, CurrencyFactory


@pytest.fixture(autouse=True)
def enable_db_for_all_tests(db):
    pass


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def base_currency():
    return CurrencyFactory(is_base_currency=True)


@pytest.fixture
def currency():
    return CurrencyFactory(is_base_currency=False, rate=70, multiplicity=100)


@pytest.fixture
def user(base_currency):
    return UserFactory(currency=base_currency)
