import pytest

from exchange_api.tests.factories import UserFactory


@pytest.fixture
def recipient(currency):
    return UserFactory(currency=currency)


def test__transfer_money(api_client, user, recipient):
    api_client.force_authenticate(user)
    amount = round(user.balance * 0.1, 2)
    sender_balance = user.balance
    recipient_balance = recipient.balance
    r = api_client.post(
        "/profile/transfer/", data={"recipient": recipient.email, "amount": amount}
    )
    assert r.status_code == 200
    operation = user.sent.first()
    assert operation
    assert operation.from_currency == user.currency
    assert operation.to_currency == recipient.currency
    assert (
        float(operation.converted_amount)
        == amount
        * user.currency.multiplicity
        * recipient.currency.rate
        / recipient.currency.multiplicity
        / user.currency.rate
    )
    user.refresh_from_db()
    assert float(user.balance) == sender_balance - amount
    recipient.refresh_from_db()
    assert recipient.balance == recipient_balance + operation.converted_amount


def test__transfer_not_enough_money(api_client, user, recipient):
    api_client.force_authenticate(user)
    r = api_client.post(
        "/profile/transfer/",
        data={"recipient": recipient.email, "amount": round(2 * user.balance, 2)},
    )
    assert r.status_code == 400
    assert "amount" in r.json()


def test__get_list_of_operations(api_client, user):
    api_client.force_authenticate(user)
    r = api_client.get("/profile/operations/")
    assert r.status_code == 200
    assert "sent" in r.json()
    assert "received" in r.json()
