from exchange_api.models import User


def test__register_user(api_client, base_currency):
    api_client.force_authenticate(None)
    r = api_client.post(
        "/register/",
        data={
            "email": "test@gmail.com",
            "password": "test_pass",
            "currency": base_currency.code,
            "balance": 100,
        },
    )
    assert r.status_code == 201
    assert User.objects.filter(email="test@gmail.com").exists()


def test_register_with_negative_balance(api_client):
    api_client.force_authenticate(None)
    r = api_client.post(
        "/register/",
        data={
            "email": "test@gmail.com",
            "password": "test_pass",
            "currency": "RUB",
            "balance": -100,
        },
    )
    assert r.status_code == 400
