from rest_framework.authtoken.models import Token


def test__user_login(api_client, user):
    api_client.force_authenticate(None)
    user.set_password("test_pass")
    user.save()
    r = api_client.post("/login/", data={"email": user.email, "password": "test_pass"})
    assert r.status_code == 200
    token = r.json()["token"]
    assert Token.objects.get(user=user).key == token
