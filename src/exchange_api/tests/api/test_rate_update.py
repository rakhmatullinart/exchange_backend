from exchange_api.services.exchange import ExchangeService


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data
    return MockResponse({"rates": {symbol: 100 for symbol in kwargs["params"]["symbols"]}}, 200)


def test__update_rates(base_currency, currency, mocker):
    mock = mocker.patch("requests.get", side_effect=mocked_requests_get)
    mock.return_value = {currency.code: 100}
    ExchangeService().update_rates()
    assert mock.called_once_with(
        "https://api.exchangeratesapi.io/latest",
        {"base": base_currency.code, "symbols": [currency.code]},
    )
    currency.refresh_from_db()
    assert float(currency.rate) == 100
