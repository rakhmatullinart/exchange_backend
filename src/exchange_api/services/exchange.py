from decimal import Decimal
from typing import List

import requests

from exchange_api.models import Currency


class ExchangeService:
    @staticmethod
    def convert(from_currency: Currency, to_currency: Currency, amount: Decimal):
        base_currency = Currency.objects.filter(is_base_currency=True).first()
        if not base_currency:
            raise LookupError("Base currency not found")
        if not from_currency.rate or not to_currency.rate:
            raise ValueError("Currency missing rate")
        return (
            amount
            * from_currency.multiplicity
            * to_currency.rate
            / to_currency.multiplicity
            / from_currency.rate
        )

    def update_rates(self):
        base_currency = Currency.objects.filter(is_base_currency=True).first()
        if not base_currency:
            return
        currencies = Currency.objects.filter(is_base_currency=False)

        rates = self.request_rates([c.code for c in currencies], base_currency.code)
        for currency_code, rate in rates.items():
            Currency.objects.filter(code=currency_code).update(rate=rate)

    @staticmethod
    def request_rates(currencies: List[str], base_currency: str):
        params = {"base": base_currency, "symbols": currencies}
        response = requests.get("https://api.exchangeratesapi.io/latest", params=params)
        if response.status_code != 200:
            return {}
        return response.json()["rates"]
