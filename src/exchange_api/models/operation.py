from django.db import models

from exchange_api.models import User, Currency


class TransferOperation(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sent")
    recipient = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="received"
    )
    from_currency = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="operations_from"
    )
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    to_currency = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="operations_to"
    )
    converted_amount = models.DecimalField(max_digits=10, decimal_places=2)
