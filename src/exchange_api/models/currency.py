import pycountry
from django.core.exceptions import ValidationError
from django.db import models


class Currency(models.Model):
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=64, verbose_name="Название валюты")
    code = models.CharField(max_length=3, verbose_name="Буквенный код валюты")
    multiplicity = models.PositiveIntegerField(verbose_name="Кратность")
    is_base_currency = models.BooleanField(default=False)
    rate = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name="Курс относительно базовой валюты",
        null=True,
    )

    def save(self, *args, **kwargs):
        currency = pycountry.currencies.get(alpha_3=self.code)
        if not currency:
            raise ValidationError(f"Unknown currency {self.code}")
        self.name = currency.name

        if (
            self.is_base_currency
            and Currency.objects.filter(is_base_currency=True).exists()
        ):
            raise ValidationError("Base currency already exists")
        if self.is_base_currency:
            self.rate = 1
            self.multiplicity = 1
        if self.rate <= 0 or self.multiplicity <= 0:
            raise ValidationError("Rate and multiplicity must be positive")
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"
