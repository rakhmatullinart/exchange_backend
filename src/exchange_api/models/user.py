from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.exceptions import ValidationError
from django.db import models, transaction

from exchange_api.models import Currency
from exchange_api.services.exchange import ExchangeService


class UserManager(BaseUserManager):
    def create_user(self, email, currency, balance, password=None):
        user = self.create(email=email, currency=currency, balance=balance)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create(email=email, balance=0)
        user.is_superuser = True
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()

    email = models.EmailField(unique=True)
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)
    balance = models.DecimalField(max_digits=10, decimal_places=2)

    USERNAME_FIELD = "email"

    @property
    def is_staff(self):
        return self.is_superuser

    def transfer(self, amount, recipient):
        if amount > self.balance:
            raise ValidationError("Not enough money on the account")
        converted_amount = amount
        if self.currency != recipient.currency:
            converted_amount = ExchangeService.convert(
                self.currency, recipient.currency, amount
            )
        self.balance -= amount
        recipient.balance += converted_amount
        with transaction.atomic():
            self.save()
            recipient.save()
        return converted_amount
