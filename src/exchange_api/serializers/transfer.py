from rest_framework import serializers

from exchange_api.models import TransferOperation, User


class TransferCreateSerializer(serializers.ModelSerializer):
    recipient = serializers.EmailField(required=True)

    class Meta:
        model = TransferOperation
        fields = ("recipient", "amount")

    def validate_recipient(self, recipient_email):
        recipient = User.objects.filter(email=recipient_email).first()
        if not recipient:
            raise serializers.ValidationError(f"{recipient_email} does not exist")
        if recipient == self.context["request"].user.email:
            raise serializers.ValidationError("Must be a different user")
        return recipient

    def validate_amount(self, amount):
        if amount < 0:
            raise serializers.ValidationError("Amount to transfer must be nonnegative")
        user = self.context["request"].user
        if user.balance < amount:
            raise serializers.ValidationError(
                f"Not enough money on the account. Available - {user.balance} {user.currency.code}"
            )
        return amount

    def create(self, validated_data):
        user = self.context["request"].user
        recipient = validated_data["recipient"]
        converted_amount = user.transfer(validated_data["amount"], recipient)
        return super().create(
            {
                "sender": user,
                "from_currency": user.currency,
                "to_currency": recipient.currency,
                "converted_amount": converted_amount,
                **validated_data,
            }
        )


class TransferOperationSerializer(serializers.ModelSerializer):
    operation_id = serializers.IntegerField(source="id", read_only=True)
    from_currency = serializers.ReadOnlyField(source="from_currency.code")
    to_currency = serializers.ReadOnlyField(source="to_currency.code")

    class Meta:
        model = TransferOperation
        fields = (
            "operation_id",
            "from_currency",
            "to_currency",
            "amount",
            "converted_amount",
        )


class TransferSendOperationSerializer(TransferOperationSerializer):
    recipient = serializers.EmailField(source="recipient.email", read_only=True)

    class Meta(TransferOperationSerializer.Meta):
        fields = TransferOperationSerializer.Meta.fields + ("recipient",)


class TransferReceiveOperationSerializer(TransferOperationSerializer):
    sender = serializers.EmailField(source="sender.email", read_only=True)

    class Meta(TransferOperationSerializer.Meta):
        fields = TransferOperationSerializer.Meta.fields + ("sender",)
