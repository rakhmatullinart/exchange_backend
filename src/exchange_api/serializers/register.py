from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from exchange_api.models import User, Currency


class RegisterSerializer(serializers.ModelSerializer):

    currency = serializers.CharField(help_text="Буквенный код валюты", max_length=3)

    class Meta:
        model = User
        fields = ("email", "password", "currency", "balance")

    def validate_password(self, password):
        validate_password(password)
        return password

    def validate_balance(self, balance):
        if balance < 0:
            raise serializers.ValidationError("Initial balance must be nonnegative")
        return balance

    def validate_currency(self, currency_code):
        currency = Currency.objects.filter(code__iexact=currency_code).first()
        if not currency:
            raise serializers.ValidationError(
                f"Currency {currency_code} does not exist in system"
            )
        return currency

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
