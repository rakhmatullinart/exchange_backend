from rest_framework import serializers

from exchange_api.models import User
from exchange_api.serializers.transfer import (
    TransferSendOperationSerializer,
    TransferReceiveOperationSerializer,
)


class UserSerializer(serializers.ModelSerializer):
    currency = serializers.ReadOnlyField(source="currency.code")

    class Meta:
        model = User
        fields = ("email", "currency", "balance")


class UserOperationSerializer(serializers.ModelSerializer):

    sent = TransferSendOperationSerializer(many=True)
    received = TransferReceiveOperationSerializer(many=True)

    class Meta:
        model = User
        fields = ("sent", "received")
