from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from exchange_api.serializers.user import UserSerializer


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, attrs):
        user = get_user_model().objects.filter(email=attrs["email"]).first()
        if not user:
            raise serializers.ValidationError(
                {"email": f"user with email {attrs['email']} does not exist"}
            )
        return attrs

    def create(self, validated_data):
        email = validated_data["email"]
        password = validated_data["password"]
        user = get_user_model().objects.get(email=email)
        if not user.check_password(password):
            raise serializers.ValidationError({"password": "wrong password"})

        return user

    def update(self, instance, validated_data):
        raise NotImplementedError


class LoginResponseSerializer(serializers.Serializer):
    token = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    def get_token(self, user):
        token, _ = Token.objects.get_or_create(user=user)
        return token.key

    @swagger_serializer_method(serializer_or_field=UserSerializer)
    def get_user(self, user):
        return UserSerializer(user, context=self.context).data

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError
