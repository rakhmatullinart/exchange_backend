from django.apps import AppConfig


class ExchangeApiConfig(AppConfig):
    name = "exchange_api"
