from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import SimpleRouter

from exchange_api.views.login import LoginAPIView
from exchange_api.views.profile import UserViewSet
from exchange_api.views.register import RegisterAPIView
from exchange_api.yasg import ExchangeSchemaGenerator, generate_api_description

urlpatterns = [
    path("login/", LoginAPIView.as_view()),
    path("register/", RegisterAPIView.as_view()),
]

router = SimpleRouter()
router.register("profile", UserViewSet, basename="profile")

urlpatterns += router.urls

# swagger schema

schema_view = get_schema_view(
    openapi.Info(
        title="Exchange API",
        default_version="v1",
        description=generate_api_description(),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    generator_class=ExchangeSchemaGenerator,
    patterns=urlpatterns,
)
urlpatterns += [
    path(
        "swagger/<format>/",
        schema_view.without_ui(cache_timeout=0),
        name="swagger-yaml",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path(
        "swagger_ui/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
]
