# Generated by Django 3.0.7 on 2020-06-22 01:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("exchange_api", "0004_auto_20200622_0049")]

    operations = [
        migrations.RemoveField(
            model_name="transferoperation", name="transfer_currency"
        ),
        migrations.DeleteModel(name="transferoperation"),
        migrations.CreateModel(
            name="TransferOperation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                (
                    "recipient",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="received",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "sender",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="sent",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "from_currency",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="operations_from",
                        to="exchange_api.Currency",
                    ),
                ),
                (
                    "to_currency",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="operations_to",
                        to="exchange_api.Currency",
                    ),
                ),
                ("amount", models.DecimalField(decimal_places=2, max_digits=10)),
                (
                    "converted_amount",
                    models.DecimalField(decimal_places=2, max_digits=10),
                ),
            ],
        ),
        migrations.AlterField(
            model_name="currency",
            name="name",
            field=models.CharField(max_length=64, verbose_name="Название валюты"),
        ),
    ]
