from exchange.celery import app
from exchange_api.services.exchange import ExchangeService


@app.task()
def update_currency_rates():
    ExchangeService().update_rates()
