from django.contrib import admin

from exchange_api.models import Currency, User

admin.site.register(Currency)
admin.site.register(User)
