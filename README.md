# exchange_backend

Система переводов - тестовое задание

# Exchange

### Ссылки
- Документация (redoc) - [http://localhost:8000/redoc/]

### Запуск проекта внутри docker контейнеров
1. `$ chmod +x ./run.sh`
2. `$ ./run.sh`


### Запуск проекта локально

Проект использует poetry для менеджмента зависимостей и virtualenv'ов



1. Установка всех зависимостей
   
       $ poetry install

2. Применение virtualenv 

       $ poetry shell
       
3. Запуск зависимостей (postgres, прочие бд)

       $ docker-compose up -d db
       $ docker-compose up -d rabbitmq
       
4. Конфигурация окружения

       $ export $(grep -v "^#" .env.local | xargs)
       
5. Запуск самого веб-сервера

       $ python3 src/manage.py runserver 0.0.0.0:8000

             
Запуск celery

       $ cd src && celery -A -B exchange -l debug worker


### Запуск тестов

Для тестирования используется pytest. 
Сначала нужно сделать шаги 1-5 из [первого пункта](#Запуск-проекта-локально).
Далее просто запустить 

    $ pytest src

